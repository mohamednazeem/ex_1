import React from 'react';
import {Card, CardImg, CardText, CardBody, CardTitle, BreadcrumbItem, Breadcrumb} from 'reactstrap';
import {Link} from 'react-router-dom';
    function RenderDish({dish}){
        if(dish != null){
            return(
                <div className="col-12 ml-5 m-1">
                     <Card>
                    <CardImg width="100%" object src={dish.image} alt={dish.name} />
                    <CardBody>
                        <CardTitle>{dish.name}</CardTitle>
                        <CardText>{dish.description}</CardText>
                    </CardBody>
                </Card> 
                </div>      
            );
        }

        else{
            return(
                <div></div>
            );
            
        }
    }

    function RenderComments({varcomment}){
        if(varcomment != null){

            const varcom = varcomment.map((comments) => {
                return(
                            <li>
                                <p> {comments.comment} </p>
                                <p> --{comments.author} </p>
                            </li>    
                );
            })
            return(
                <div>
                    <h4>comments</h4>   
                    <ul key={varcom.id} className="list-unstyled">
                        {varcom}
                    </ul>
                </div>
            );

            }
            else{
                return(
                    <div></div>
                );
            }
           
        }

        const DishDetail = (props) => {
            if(props.dishs){
                return(
                    <div className="container">
                        <div className="row">
                            <Breadcrumb>
                                <BreadcrumbItem><Link to='/menu'>Menu</Link></BreadcrumbItem>
                                <BreadcrumbItem active>{props.dishs.name}</BreadcrumbItem>
                            </Breadcrumb>
                            <div className="col-12">
                                <h3>{props.dishs.name}</h3>
                                <hr />
                            </div>
                        </div>
                          <div  className="row">
                              <div className="col-12 col-md-5 m-1">
                                <RenderDish dish={props.dishs}/> 
                              </div>
                            <div className="col-12 col-md-5 m-1">
                                 <RenderComments varcomment={props.Dcomment} />
                            </div>
                            
                          </div>
                    </div>
                    
                    
                );
            }
            else{
                return(
                    <div></div>
                );
            }
        }   
            
        
        
    

   


export default DishDetail;